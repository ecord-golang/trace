package trace_test

import (
	"errors"
	"fmt"
	"trace"
)

func Example_here() {
	c := func() {
		fmt.Printf("%s\n%s", "Hello from:", trace.Here(trace.LLine | trace.LFunction))
	}

	b := func() { c() }
	a := func() { b() }

	a()

	// Output:
	// Hello from:
	// :11 trace_test.Example_here.func1
}

func Example_here_second() {
	err := errors.New("internal system error")

	fmt.Printf("%s %s\n%s", "Error:", err, trace.Here(trace.LLine | trace.LFunction))

	// Output:
	// Error: internal system error
	// :27 trace_test.Example_here_second
}

func Example_fromParent() {
	c := func() {
		fmt.Printf("%s\n%s", "Hello from:", trace.FromParent(3, trace.LLine | trace.LFunction))
	}

	b := func() { c() }
	a := func() { b() }

	a()

	// Output:
	// Hello from:
	// :42 trace_test.Example_fromParent
}