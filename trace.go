package trace

import (
	"bytes"
	"fmt"
	"io"
	"runtime"
)

// Output flags
const (
	LFile     = 1 << iota         // full file name
	LLine                         // line number
	LFunction                     // name of the function
	LStdFlags = LFile | LFunction // initial values
)

// Here returns string representation of a reference
// default flags LStdFlags
func Here(flags int) string {
	return FromParent(1, flags)
}

// FromParent returns string representation of a parent reference
// default flags LStdFlags
func FromParent(callDepth int, flags int) string {
	var buf bytes.Buffer

	frame := getFrame(callDepth + 2)
	outputFrame(flags, frame, &buf)

	return buf.String()
}

func outputFrame(flags int, frame *runtime.Frame, w io.Writer) {
	if flags == 0 {
		flags = LStdFlags
	}

	if frame != nil {
		if flags&LFile != 0 {
			_, _ = fmt.Fprintf(w, "%s", frame.File)
		}
		if flags&LLine != 0 {
			_, _ = fmt.Fprintf(w, ":%d", frame.Line)
		}
		if flags&LFunction != 0 {
			_, _ = fmt.Fprintf(w, " %s", frame.Function)
		}
	}
}

func getFrame(callDepth int) *runtime.Frame {
	pc, file, line, ok := runtime.Caller(callDepth)
	if !ok {
		return nil
	}

	frame := &runtime.Frame{
		PC:   pc,
		File: file,
		Line: line,
	}

	funcForPc := runtime.FuncForPC(pc)
	if funcForPc != nil {
		frame.Func = funcForPc
		frame.Function = funcForPc.Name()
		frame.Entry = funcForPc.Entry()
	}

	return frame
}