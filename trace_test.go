package trace

import (
	"testing"
)

func TestFlags(t *testing.T) {
	ref := Here(LFile | LFunction)
	if ref == "" {
		t.Fatalf("Did not trace the call (%s)", ref)
	}

	parentRef := FromParent(1, LFile | LFunction)
	if parentRef == "" {
		t.Fatalf("Did not trace the call (%s)", parentRef)
	}
}

func TestFromParent(t *testing.T) {
	type args struct {
		callDepth int
		flags     int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"trace with one flag", args{0, LLine}, ":34"},
		{"trace with multiple flags", args{0, LLine | LFunction}, ":34 trace.TestFromParent.func1"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FromParent(tt.args.callDepth, tt.args.flags); got != tt.want {
				t.Errorf("FromParent() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHere(t *testing.T) {
	type args struct {
		flags int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"trace with one flag", args{LLine}, ":55"},
		{"trace with multiple flags", args{LLine | LFunction}, ":55 trace.TestHere.func1"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Here(tt.args.flags); got != tt.want {
				t.Errorf("Here() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTraceDefaultFlags(t *testing.T) {
	ref := Here(0)
	if ref == "" {
		t.Fatalf("Did not trace the call (%s)", ref)
	}
}